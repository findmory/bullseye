//
//  ViewController.swift
//  bullseye
//
//  Created by Mike Ory on 12/10/18.
//  Copyright © 2018 findmory development. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	// instance variable to hold the integer value of the slider
	var currentValue: Int = 0
	var targetValue: Int = 0
	var roundNumber: Int = 0
	var totalScore = 0 // not specifying 'Int' because i'm using type inferrence
	var highScore: Int = 0
	
	// connect 'slider' var to the actual value of the slider object
	@IBOutlet weak var slider: UISlider!
	@IBOutlet weak var targetLabel: UILabel!
	@IBOutlet weak var totalScoreLabel: UILabel!
	@IBOutlet weak var roundNumLabel: UILabel!
	@IBOutlet weak var highScoreLabel: UILabel!
	@IBOutlet weak var gameOverLabel: UILabel!
	@IBOutlet weak var gameButton: UIButton!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// when view loads get the current slider value as int and store in currentValue
		let roundedValue: Float = slider.value.rounded()
		currentValue = Int(roundedValue)
		
		// increment round number initialize the target value to a random number
		self.startNewRound()
		
		let thumbImageNormal = #imageLiteral(resourceName: "SliderThumb-Normal")
		slider.setThumbImage(thumbImageNormal, for: .normal)
		
		let insets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
		let trackLeftImage = #imageLiteral(resourceName: "SliderTrackLeft")
		let trackLeftResizable = trackLeftImage.resizableImage(withCapInsets: insets)
		slider.setMinimumTrackImage(trackLeftResizable, for: .normal)
		
		let trackRightImage = #imageLiteral(resourceName: "SliderTrackRight")
		let trackRightResizable = trackRightImage.resizableImage(withCapInsets: insets)
		slider.setMaximumTrackImage(trackRightResizable, for: .normal)
		
		// load the high score from UserDefaults
		let highScoreDisplay = UserDefaults.standard.string(forKey: "highscore") ?? "0"
		highScore = Int(highScoreDisplay)!
		highScoreLabel.text = highScoreDisplay
	}
	
	
	@IBAction func showAlert() {
		// unpack the tuple that's returned from calculateScore(0
		let (title, message) = calculateScore()
		
		let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
		
		// this is the button inside the alert
		// note the closure being called for the handler (aka Callback)
		let action = UIAlertAction(title: "Next Round", style: .default, handler: {
			action in
			self.startNewRound()
		})
		
		alert.addAction(action)
		
		// this is async.  so the startNewRound() gets run before the alert is closed, so add a closure to
		// the handler (above)
		// remember a closure is a function without name
		present(alert, animated: true, completion: nil)
	}
	
	
	@IBAction func sliderMoved(_ slider: UISlider) {
		let roundedValue: Float = slider.value.rounded()
		currentValue = Int(roundedValue)
	}
	
	
	@IBAction func startOver(_ sender: UIButton) {
		roundNumber = 0
		totalScore = 0
		// clear the `game over` message
		gameOverLabel.isHidden = true
		gameButton.isEnabled = true
		self.startNewRound()
	}
	
	
	func startNewRound() {
		roundNumber += 1
		if roundNumber <= 10 {
			// rule out 1 and 99 because it's too easy
			targetValue = Int.random(in: 2...99)
			slider.value = 50
		} else {
			roundNumber = 10 // for display
			self.endGame()
		}
		updateLabels()
		
	}
	
	
	func updateLabels() {
		targetLabel.text = String(targetValue)
		totalScoreLabel.text = String(totalScore)
		roundNumLabel.text = String(roundNumber)
	}
	
	func endGame() {
		// turn on the `game over` message
		gameOverLabel.isHidden = false
		gameButton.isEnabled = false
		
		
		// set high score if it's higher than previous
		if totalScore > highScore {
			UserDefaults.standard.set(String(totalScore), forKey: "highscore")
			highScore = totalScore
			highScoreLabel.text = String(highScore)
		}
	}
	
	
	func calculateScore() -> (String, String) {
		let diff: Int = abs(targetValue - currentValue)
		var points: Int = 0
		let title: String
		
		if diff == 0 {
			title = "BULLSEYE!"
			points = 150
		} else if diff == 1 {
			title = "Sooo close!"
			points = 100
		} else if diff <= 5 {
			title = "Pretty close!"
			points = 75
		} else if diff <= 10{
			title = "Not bad"
			points = 25
		} else {
			title = "Uh.. not even close"
		}
		
		let message = "->\(currentValue)<- \nWorth \(points) points"
		totalScore += points
		
		return (title, message)
	}
	
}

