//
//  AboutViewController.swift
//  bullseye
//
//  Created by Mike Ory on 12/13/18.
//  Copyright © 2018 findmory development. All rights reserved.
//

import UIKit
import WebKit

class AboutViewController: UIViewController {
	
	@IBOutlet weak var webView: WKWebView!

	override func viewDidLoad() {
		super.viewDidLoad()

		if let htmlPath = Bundle.main.path(forResource: "BullsEye", ofType: "html") {
			let url = URL.init(fileURLWithPath: htmlPath)
			let request = URLRequest(url: url)
			webView.load(request)
		}
	}
	
	@IBAction func close() {
		dismiss(animated: true, completion: nil)
	}

}

