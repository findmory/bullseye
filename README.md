# BullsEye - Batter's Eye

BullsEye is a simple slider game where you score points by moving the slider as close as you can to the target value.

![game](./images/game_over.png)

Each game lasts for 10 levels and the high score is persisted across games.

![scoring](./images/scoring.PNG)
